// console.log("Hello batch 224!");

// Functions
	// Function in JS are line/block of codes that tell your device/application to perform a certain task when called/invoked.
	// Functions are mostly created to create complicated tasks to run several lines of code in succession.
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function.

// Function Declarations
	// (Function statements) -defines a function with the specified parameters.

	/*
		Syntax:
		function functionName() {
			code block (statement);
		};
	*/

	// Function Keyword - used to define a JS function.
	// functionName - Functions are named to be able to use later in the program/code.
	//function block ({}) - the statements which comprise the body of the function. This is where the code to be executed is found.

		function printName() {
			console.log("My name is Rupert");
		};

		// Semicolons are used to separate executable JS statements.
		printName();
		printName();

// Function Invocation
	// The code block and statements inside a function is not immeidately executed when the function is defined. The code block and statements inside a function is only executed when the function is invoked or called.

		// This is how we invoke/call the function that we declared.
		printName();

		// declaredFunction(); - results in an error, much like variables, we cannot invoke a function we have not yet defined.

// Function Declaration vs. Function Expression

	// Function Declaration

		// A function can be created through function declaration by using the function keyword and adding function name

		// Declared functions are not executed immediately.

		declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.

		// Note: Hoisting in JS's behavior for certain variables and function to run and use the, before their declaration.

		function declaredFunction() {
			console.log("Hello World from declaredFunction()");
		};	

		declaredFunction();

		// Function Expression
			// A function can also be stored in a variable. This is called function expression

			// A funtion expression is an anonymous function assigned to a variableFunction

			// Anonymousfunction - a function without a name.

			// variableFunction();
			/*
				result to an error - function expression, being stored in a let/const declaration, cannot be hoisted.
			*/

			let variableFunction = function() {
				console.log("Hello Again!");
			};

			variableFunction();


			// We can also create a function expression of a named function.
			// However, to invoke the function expression, we invoke it by its variable name and not by its function name.

			let funcExpression = function funcName() {
				console.log("Hello from the other side.");
			};

			// funcName();
			funcExpression();


			// You can reassign declared functions and function expressions to a new anonymous functions.

			declaredFunction();

			declaredFunction = function() {
				console.log("Updated declaredFuntion");
			};

			declaredFunction();

			funcExpression = function() {
				console.log("updated funcExpression");
			};

			funcExpression();

			declaredFunction();



			// However, we cannot re-assign a function expression initialized with const kerword.

			const constantFunc = function() {
				console.log("This is initialized with const!");
			};

			constantFunc();

/*
			constantFunc = function(){
				console.log("Cannot be re-assigned!");
			};

			constantFunc();

*/		



// Function Scoping
/*
	Scope is the accessibility ( visibility) of variables within our program.

	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope	
*/
		

		// Block Scope
			{
				let localVar = "Demi Lavato";
				console.log(localVar);
			};

			// console.log(localVar); - this is going to result in an error.

		// Global Scope

		let globalVar = "Mr. Worldwide";
		console.log(globalVar);

		// Function Scope

		/*
			Javascript has a function scope: Each function creates a new scope.
			Variable defined inside a function are not accessible or visible from outside that function.
		*/

			function showNames() {

				// Function scoped variables:
				var functionVar = "Joe";
				const functionConst = "John";
				let functionLet= "Jane";	

				console.log(functionVar);		
				console.log(functionConst);		
				console.log(functionLet);
				console.log(globalVar);		
			};

			showNames();

				// All these will result into an error
				// console.log(functionVar);		
				// console.log(functionConst);		
				// console.log(functionLet);

			/*
				The variables functionVar, functionConst and functionLet are function scoped and cannot be accessed outside of the function they were declared in.

			*/

// Nested Functions

	// You can create another function inside a function. This is called a nested function. This nested function, being inside the parent function will have access to the variable as they are within the same scope/code block.

		function myNewFunctio() {
			let name = "Jessica";

			function nestedFunction() {
				let nestedName = "Joseph";
				console.log(name);
			};

			// console.log(nestedName); // results to an error
			nestedFunction();
		};

		myNewFunctio();

		// nestedFunction(); // results to an error


// Function and Global Scope Variables

	function myNewFunction2() {
		let nameInside = "Mr. Function Scope";
		console.log(globalVar);
	}

	myNewFunction2();

	// console.log(nameInside); // results to an error because this is a funciton scoped variable.

